import java.awt.Point;
import java.io.*;
import java.util.*;
public class Keyboarding {
    public static void main(String[] args) throws Exception{
        Scanner sc = new Scanner(System.in);
        int r = sc.nextInt(), c = sc.nextInt();
        char[][] keyboard = new char[r][c];
        for(int i = 0; i < r; i++){
            String temp = sc.next();
            for(int j = 0; j < c; j++){
                keyboard[i][j] = temp.charAt(j);
            }
        }
        String word = sc.next() + "*";
                
        Loc graph[][] = new Loc[r][c];
        int[] dr = {0, 1, 0, -1}; int[] dc = {1, 0, -1, 0};
        for(int i = 0; i < r; i++){
            for(int j = 0; j < c; j++){
                char cur = keyboard[i][j];
                Loc loc = new Loc(cur, word.length());
                for(int d = j; d >= 0; d--){//N
                    if(keyboard[i][d]!= cur){
                        loc.pointsTo.add(new Point(i, d)); break;
                    }
                }
                for(int d = i; d < r; d++){//E
                    if(keyboard[d][j] != cur){
                        loc.pointsTo.add(new Point(d, j)); break;
                    }
                }
                for(int d = j; d < c; d++){//S
                    if(keyboard[i][d] != cur){
                        loc.pointsTo.add(new Point(i, d)); break;
                    }
                }
                for(int d = i; d >= 0; d--){//W
                    if(keyboard[d][j] != cur){
                        loc.pointsTo.add(new Point(d, j)); break;
                    }
                }
                graph[i][j] = loc;
            }
        }
        
        int currentKey = 0; int moves = 0;  
        Queue<Loc> keySearch = new LinkedList<>();
        keySearch.add(graph[0][0]); int winner = Integer.MAX_VALUE;
        while(!keySearch.isEmpty()){
            Loc temp = keySearch.remove();
            moves = temp.moves; currentKey = temp.keysPressed+1;
            ArrayList<Loc> toSearch = new ArrayList<>();
            if(currentKey == word.length()){
                if(moves < winner) winner = moves;                
            }
            else toSearch.add(temp);
            while(!toSearch.isEmpty()){
                ArrayList<Loc> nextSearch = new ArrayList<>();
                for (Loc cur : toSearch) {
                    if(cur.letter == word.charAt(currentKey)){
                        cur.moves = moves; cur.keysPressed = currentKey;
                        keySearch.add(cur); cur.visited[currentKey] = true;
                    }
                    else if(!cur.visited[currentKey]){
                        for(Point p : cur.pointsTo){
                            nextSearch.add(graph[p.x][p.y]);
                            //graph[p.x][p.y].visited[currentKey] = true;
                        }
                        cur.visited[currentKey] = true;
                    }               
                }
                moves++;
                toSearch = nextSearch;
            }
        }
        System.out.println(moves);
    }
}

class Loc{
    char letter; int moves = 0, keysPressed = 0;
    Set<Point> pointsTo = new HashSet<>();
    boolean visited[];
    Loc(char x, int n){
        this.letter = x; this.visited = new boolean[n];
    }
}