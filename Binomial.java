import java.util.*;
import java.io.*;
public class Binomial {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double x = sc.nextInt(); double n = sc.nextInt(); double p = sc.nextDouble(); double sum = 0.0;
        for(int i = (int)x; i < (int)n; i++){
            sum += (Math.pow(p, i))*(Math.pow(1-p, (x-i)));
        }
        System.out.println(sum);
    }
}
