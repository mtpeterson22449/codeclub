import java.io.*;
public class RockPaperScissors {
	public static void main(String[] args) throws Exception {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		while(true){
			String input = in.readLine().trim();
			if(input.equals("0")) break;
			String[] l1 = input.split(" ");
			int players = Integer.parseInt(l1[0]), games = Integer.parseInt(l1[1]);
			int[] wins = new int[players], losses = new int[players];
			
			for(int i = 0; i < (games*players*(players-1)/2); i++){
				String[] line = in.readLine().split(" ");
				int p1 = Integer.parseInt(line[0])-1, p2 = Integer.parseInt(line[2])-1;
				if(line[1].equals(line[3])) continue;//if the moves are the same
				if(line[1].equals("rock")){
					if(line[3].equals("scissors")){
						wins[p1]++;	losses[p2]++;
					}
					else{
						wins[p2]++; losses[p1]++;
					}
				}
				else if(line[1].equals("paper")){
					if(line[3].equals("rock")){
						wins[p1]++;	losses[p2]++;
					}
					else{
						wins[p2]++; losses[p1]++;
					}
				}
				else if(line[1].equals("scissors")){
					if(line[3].equals("paper")){
						wins[p1]++;	losses[p2]++;
					}
					else{
						wins[p2]++; losses[p1]++;
					}
				}
			}
			for(int i = 0; i < players; i++){
				if(wins[i] + losses[i] == 0) System.out.println("-");
				else System.out.printf("%.3f\n",(double)wins[i]/(wins[i]+losses[i]));
			}
			System.out.println("");
		}
	}
}
